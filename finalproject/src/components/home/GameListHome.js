import React, { useContext, useEffect } from "react";
import { GameContext } from "../game/GameContext";
import axios from "axios";

const GameListHome = () => {
  const [game, setGame] = useContext(GameContext);

  useEffect(() => {
    if (game === null) {
      axios
        .get(`https://backendexample.sanbersy.com/api/data-game`)
        .then((res) => {
          setGame(res.data);
        });
    }
  }, [game, setGame]);

  return (
    <div>
      <div style={{ marginTop: 100 }}></div>
      <div
        style={{
          backgroundColor: "white",
          width: 1200,
          paddingLeft: 50,
          paddingRight: 50,
          display: "inline-block",
          textAlign: "center",
        }}
      >
        <h1>Daftar Game-game Terbaik</h1>
        {game !== null &&
          game.map((el, idx) => {
            return (
              <div>
                <h3 style={{ textAlign: "left" }}>{el.name}</h3>
                <div style={{ display: "inline-flex" }}>
                  <table>
                    <tr>
                      <td>
                        <img
                          style={{
                            width: 950,
                            height: 500,
                            objectFit: "cover",
                            flex: 4,
                          }}
                          src={el.image_url}
                          id="logo"
                          alt="alt"
                        />
                      </td>
                      <td>
                        <div
                          style={{ flex: 1, padding: 50, textAlign: "left" }}
                        >
                          <p>
                            <strong>Platform: {el.platform}</strong>
                          </p>
                          <p>
                            <strong>Release: {el.release}</strong>
                          </p>
                          <p>
                            <strong>Genre: {el.genre}</strong>
                          </p>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
                <p style={{ textAlign: "left" }}>
                  <strong>Single Player</strong> {el.singlePlayer}
                </p>
                <p style={{ textAlign: "left" }}>
                  <strong>Multi Player</strong> {el.multiplayer}
                </p>
                {/* <div style={{ borderColor: "black", borderTop: 1 }}></div> */}
                <hr size="10px"></hr>
              </div>
            );
          })}
      </div>
      <div style={{ marginBottom: 100 }}></div>
    </div>
  );
};

export default GameListHome;
