import React from "react";
import { MovieProvider } from "../movie/MovieContext";
import MovieListHome from "./MovieListHome";
import { GameProvider } from "../game/GameContext";
import GameListHome from "./GameListHome";

const Home = () => {
  //   const tema = useContext(TemaContext);

  return (
    <div>
      <MovieProvider>
        <MovieListHome />
      </MovieProvider>
      <GameProvider>
        <GameListHome />
      </GameProvider>
    </div>
  );
};

export default Home;
