import React, { useContext, useEffect } from "react";
import { MovieContext } from "./MovieContext";
import { UserContext } from "../auth/UserContext";
import axios from "axios";

const MovieList = () => {
  const [movie, setMovie, inputForm, setInputForm] = useContext(MovieContext);
  const [user] = useContext(UserContext);
  //   const [search, setSearch] = useState("");
  //   const [filteredMovie, setFilterMovie] = useState([]);

  useEffect(() => {
    if (movie === null) {
      axios
        .get(`https://backendexample.sanbersy.com/api/data-movie`)
        .then((res) => {
          setMovie(res.data);
        });
    }
  }, [movie, setMovie]);

  //   useEffect(() => {
  //     if (movie !== null) {
  //       setFilterMovie(
  //         movie.filter((res) =>
  //           res.title.toLowercase().include(search.toLowercase())
  //         )
  //       );
  //     }
  //   }, [search, movie]);

  const handleDelete = (event) => {
    let idMovie = parseInt(event.target.value);
    axios
      .delete(`https://backendexample.sanbersy.com/api/data-movie/${idMovie}`, {
        headers: { Authorization: `Bearer ${user.token}` },
      })
      .then((res) => {
        let newDataMovie = movie.filter((x) => x.id !== idMovie);
        setMovie(newDataMovie);
      });
  };

  const handleEdit = (event) => {
    var idMovie = parseInt(event.target.value);
    var singleMovie = movie.find((x) => x.id === idMovie);
    setInputForm({
      ...inputForm,
      title: singleMovie.title,
      description: singleMovie.description,
      year: singleMovie.year,
      duration: singleMovie.duration,
      genre: singleMovie.genre,
      rating: singleMovie.rating,
      review: singleMovie.review,
      image_url: singleMovie.image_url,
      id: idMovie,
    });
  };

  return (
    <div>
      <div style={{ marginTop: 100 }}></div>
      {/* <div>
        <p>
          Search :{" "}
          <input
            type="text"
            placeholder="Search Movie Title"
            // onChange={(e) => setSearch(e.target.value)}
          />{" "}
          <button>submit</button>
        </p>
      </div>
      <div style={{ marginTop: 50 }}></div> */}
      <div
        style={{
          backgroundColor: "white",
          width: 1400,
          padding: 50,
          display: "inline-block",
          textAlign: "left",
        }}
      >
        <h1>Movies List</h1>
        <table>
          <thead>
            <tr>
              <th>No</th>
              <th>Title</th>
              <th>Description</th>
              <th>Year</th>
              <th>Duration</th>
              <th>Genre</th>
              <th>Rating</th>
              {user !== null && <th>Action</th>}
            </tr>
          </thead>
          <tbody>
            {/* if (search === null) */}
            {movie !== null &&
              movie.map((el, idx) => {
                return (
                  <tr key={el.id}>
                    <td>{idx + 1}</td>
                    <td>{el.title}</td>
                    <td>{el.description}</td>
                    <td>{el.year}</td>
                    <td>{el.duration}</td>
                    <td>{el.genre}</td>
                    <td>{el.rating}</td>
                    {user !== null && (
                      <td>
                        <button
                          value={el.id}
                          style={{ marginRight: "10px" }}
                          onClick={handleEdit}
                        >
                          Edit
                        </button>
                        <button value={el.id} onClick={handleDelete}>
                          Delete
                        </button>
                      </td>
                    )}
                  </tr>
                );
              })}
            {/* else
          {filteredMovie.map((el, idx) => {
            return (
              <tr key={el.id}>
                <td>{idx + 1}</td>
                <td>{el.title}</td>
                <td>{el.description}</td>
                <td>{el.year}</td>
                <td>{el.duration}</td>
                <td>{el.genre}</td>
                <td>{el.rating}</td>
                <td>
                  <button
                    value={el.id}
                    style={{ marginRight: "10px" }}
                    onClick={handleEdit}
                  >
                    Edit
                  </button>
                  <button value={el.id} onClick={handleDelete}>
                    Delete
                  </button>
                </td>
              </tr>
            ); */}
          </tbody>
        </table>
      </div>
      <div style={{ marginBottom: 100 }}></div>
    </div>
  );
};

export default MovieList;
