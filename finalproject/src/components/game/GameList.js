import React, { useContext, useEffect } from "react";
import { GameContext } from "./GameContext";
import { UserContext } from "../auth/UserContext";
import axios from "axios";

const GameList = () => {
  const [game, setGame, inputForm, setInputForm] = useContext(GameContext);
  const [user] = useContext(UserContext);
  //   const [search, setSearch] = useState("");
  //   const [filteredGame, setFilterGame] = useState([]);

  useEffect(() => {
    if (game === null) {
      axios
        .get(`https://backendexample.sanbersy.com/api/data-game`)
        .then((res) => {
          setGame(res.data);
        });
    }
  }, [game, setGame]);

  //   useEffect(() => {
  //     if (Game !== null) {
  //       setFilterGame(
  //         Game.filter((res) =>
  //           res.title.toLowercase().include(search.toLowercase())
  //         )
  //       );
  //     }
  //   }, [search, Game]);

  const handleDelete = (event) => {
    let idGame = parseInt(event.target.value);
    axios
      .delete(`https://backendexample.sanbersy.com/api/data-game/${idGame}`, {
        headers: { Authorization: `Bearer ${user.token}` },
      })
      .then((res) => {
        let newDataGame = game.filter((x) => x.id !== idGame);
        setGame(newDataGame);
      });
  };

  const handleEdit = (event) => {
    var idGame = parseInt(event.target.value);
    var singleGame = game.find((x) => x.id === idGame);
    setInputForm({
      ...inputForm,
      id: idGame,
      name: singleGame.name,
      genre: singleGame.genre,
      singlePlayer: singleGame.singlePlayer,
      multiplayer: singleGame.multiplayer,
      platform: singleGame.platform,
      release: singleGame.release,
      image_url: singleGame.image_url,
    });
  };

  return (
    <div>
      <div style={{ marginTop: 100 }}></div>
      {/* <div>
        <p>
          Search :{" "}
          <input
            type="text"
            placeholder="Search Game Title"
            // onChange={(e) => setSearch(e.target.value)}
          />{" "}
          <button>submit</button>
        </p>
      </div>
      <div style={{ marginTop: 50 }}></div> */}
      <div
        style={{
          backgroundColor: "white",
          width: 1400,
          padding: 50,
          display: "inline-block",
          textAlign: "left",
        }}
      >
        <h1>Games List</h1>
        <table>
          <thead>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>Genre</th>
              <th>Single Player</th>
              <th>Multi Player</th>
              <th>Platform</th>
              <th>Release</th>
              {user !== null && <th>Action</th>}
            </tr>
          </thead>
          <tbody>
            {/* if (search === null) */}
            {game !== null &&
              game.map((el, idx) => {
                return (
                  <tr key={el.id}>
                    <td>{idx + 1}</td>
                    <td>{el.name}</td>
                    <td>{el.genre}</td>
                    <td>{el.singlePlayer}</td>
                    <td>{el.multiplayer}</td>
                    <td>{el.platform}</td>
                    <td>{el.release}</td>
                    {user !== null && (
                      <td>
                        <button
                          value={el.id}
                          style={{ marginRight: "10px" }}
                          onClick={handleEdit}
                        >
                          Edit
                        </button>
                        <button value={el.id} onClick={handleDelete}>
                          Delete
                        </button>
                      </td>
                    )}
                  </tr>
                );
              })}
            {/* else
          {filteredGame.map((el, idx) => {
            return (
              <tr key={el.id}>
                <td>{idx + 1}</td>
                <td>{el.title}</td>
                <td>{el.description}</td>
                <td>{el.year}</td>
                <td>{el.duration}</td>
                <td>{el.genre}</td>
                <td>{el.rating}</td>
                <td>
                  <button
                    value={el.id}
                    style={{ marginRight: "10px" }}
                    onClick={handleEdit}
                  >
                    Edit
                  </button>
                  <button value={el.id} onClick={handleDelete}>
                    Delete
                  </button>
                </td>
              </tr>
            ); */}
          </tbody>
        </table>
      </div>
      <div style={{ marginBottom: 100 }}></div>
    </div>
  );
};

export default GameList;
