import React, { useContext, useEffect } from "react";
import { GameContext } from "./GameContext";
import { UserContext } from "../auth/UserContext";
import axios from "axios";

const GameForm = () => {
  const [game, setGame, inputForm, setInputForm] = useContext(GameContext);
  const [user] = useContext(UserContext);

  useEffect(() => {
    if (game === null) {
      axios
        .get(`https://backendexample.sanbersy.com/api/data-game`)
        .then((res) => {
          setGame(res.data);
        });
    }
  }, [game, setGame]);

  const handleSubmit = (event) => {
    event.preventDefault();
    // var newId = buah.length + 1;
    if (inputForm.id === null) {
      axios
        .post(
          `https://backendexample.sanbersy.com/api/data-game`,
          {
            name: inputForm.name,
            genre: inputForm.genre,
            singlePlayer: inputForm.singlePlayer,
            multiplayer: inputForm.multiplayer,
            platform: inputForm.platform,
            release: inputForm.release,
            image_url: inputForm.image_url,
          },
          { headers: { Authorization: `Bearer ${user.token}` } }
        )
        .then((res) => {
          let data = res.data;
          // alert(data.id);
          setGame([
            ...game,
            {
              id: data.id,
              name: data.name,
              genre: data.genre,
              singlePlayer: data.singlePlayer,
              multiplayer: data.multiplayer,
              platform: data.platform,
              release: data.release,
              image_url: data.image_url,
            },
          ]);
          setInputForm({
            id: null,
            name: "",
            genre: "",
            singlePlayer: 0,
            multiplayer: 0,
            platform: "",
            release: "",
            image_url: "",
          });
        });
    } else {
      axios
        .put(
          `https://backendexample.sanbersy.com/api/data-game/${inputForm.id}`,
          {
            name: inputForm.name,
            genre: inputForm.genre,
            singlePlayer: inputForm.singlePlayer,
            multiplayer: inputForm.multiplayer,
            platform: inputForm.platform,
            release: inputForm.release,
            image_url: inputForm.image_url,
          },
          { headers: { Authorization: `Bearer ${user.token}` } }
        )
        .then((res) => {
          let newDataGame = game.map((x) => {
            if (x.id === inputForm.id) {
              x.name = inputForm.name;
              x.genre = inputForm.genre;
              x.singlePlayer = inputForm.singlePlayer;
              x.multiplayer = inputForm.multiplayer;
              x.platform = inputForm.platform;
              x.release = inputForm.release;
              x.image_url = inputForm.image_url;
            }
            return x;
          });
          setGame(newDataGame);
          setInputForm({
            id: null,
            name: "",
            genre: "",
            singlePlayer: 0,
            multiplayer: 0,
            platform: "",
            release: "",
            image_url: "",
          });
        });
    }
  };

  const handleChangeName = (event) => {
    setInputForm({ ...inputForm, name: event.target.value });
  };

  const handleChangeGenre = (event) => {
    setInputForm({ ...inputForm, genre: event.target.value });
  };

  const handleChangeSinglePlayer = (event) => {
    setInputForm({ ...inputForm, singlePlayer: event.target.value });
  };

  const handleChangeMultiplayer = (event) => {
    setInputForm({ ...inputForm, multiplayer: event.target.value });
  };

  const handleChangePlatform = (event) => {
    setInputForm({ ...inputForm, platform: event.target.value });
  };

  const handleChangeRelease = (event) => {
    setInputForm({ ...inputForm, release: event.target.value });
  };

  const handleChangeImageUrl = (event) => {
    setInputForm({ ...inputForm, image_url: event.target.value });
  };

  return (
    <>
      {user !== null && (
        <>
          <div
            style={{
              backgroundColor: "white",
              width: 1400,
              padding: 50,
              display: "inline-block",
              textAlign: "center",
            }}
          >
            <h1>Games Form</h1>
            <form onSubmit={handleSubmit}>
              <div
                style={{
                  display: "inline-block",
                  textAlign: "center",
                  width: 700,
                }}
              >
                <table style={{ width: "80%" }}>
                  <tr>
                    <td style={{ textAlign: "left" }}>
                      <strong>Name : </strong>
                    </td>
                    <td style={{ textAlign: "right" }}>
                      <input
                        type="text"
                        value={inputForm.name}
                        onChange={handleChangeName}
                        size="30"
                      />
                    </td>
                  </tr>
                  <tr>
                    <td style={{ textAlign: "left" }}>
                      <strong>Genre : </strong>
                    </td>
                    <td style={{ textAlign: "right" }}>
                      <textarea
                        value={inputForm.genre}
                        onChange={handleChangeGenre}
                        rows="4"
                        cols="25"
                      />
                    </td>
                  </tr>
                  <tr>
                    <td style={{ textAlign: "left" }}>
                      <strong>Single Player : </strong>
                    </td>
                    <td style={{ textAlign: "right" }}>
                      <input
                        type="number"
                        value={inputForm.singlePlayer}
                        onChange={handleChangeSinglePlayer}
                        size="10p"
                        style={{ width: 90 }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <td style={{ textAlign: "left" }}>
                      <strong>Multi Player : </strong>
                    </td>
                    <td style={{ textAlign: "right" }}>
                      <input
                        type="number"
                        value={inputForm.multiplayer}
                        onChange={handleChangeMultiplayer}
                        style={{ width: 200 }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <td style={{ textAlign: "left" }}>
                      <strong>Platform : </strong>
                    </td>
                    <td style={{ textAlign: "right" }}>
                      <input
                        type="text"
                        value={inputForm.platform}
                        onChange={handleChangePlatform}
                        size="10"
                      />
                    </td>
                  </tr>
                  <tr>
                    <td style={{ textAlign: "left" }}>
                      <strong>Release : </strong>
                    </td>
                    <td style={{ textAlign: "right" }}>
                      <input
                        type="text"
                        value={inputForm.release}
                        onChange={handleChangeRelease}
                        style={{ width: 40 }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <td style={{ textAlign: "left" }}>
                      <strong>Image Url : </strong>
                    </td>
                    <td style={{ textAlign: "right" }}>
                      <textarea
                        type="text"
                        value={inputForm.image_url}
                        onChange={handleChangeImageUrl}
                        row="4"
                        cols="55"
                      />
                    </td>
                  </tr>
                  <tr>
                    <td></td>
                    <td style={{ textAlign: "right" }}>
                      <button>submit</button>
                    </td>
                  </tr>
                </table>
              </div>
            </form>
          </div>
        </>
      )}
    </>
  );
};

export default GameForm;
