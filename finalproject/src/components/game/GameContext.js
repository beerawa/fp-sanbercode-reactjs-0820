import React, { useState, createContext } from "react";
// import axios from "axios";
// import "./DaftarBuah.css";

export const GameContext = createContext();

export const GameProvider = (props) => {
  const [game, setGame] = useState(null);

  const [inputForm, setInputForm] = useState({
    // name: "",
    // lengthOfTime: 0,
    id: null,
    name: "",
    genre: "",
    singlePlayer: 0,
    multiplayer: 0,
    platform: "",
    release: "",
    image_url: "",
  });

  return (
    <GameContext.Provider value={[game, setGame, inputForm, setInputForm]}>
      {props.children}
    </GameContext.Provider>
  );
};
