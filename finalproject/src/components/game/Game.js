import React from "react";
import { GameProvider } from "./GameContext";
import GameList from "./GameList";
import GameForm from "./GameForm";

const Game = () => {
  //   const tema = useContext(TemaContext);

  return (
    <div>
      <div style={{ marginTop: 100 }}></div>
      <GameProvider>
        <GameList />
        <GameForm />
      </GameProvider>
      <div style={{ marginBottom: 100 }}></div>
    </div>
  );
};

export default Game;
