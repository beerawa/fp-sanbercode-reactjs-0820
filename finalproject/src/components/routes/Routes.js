import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// import Home from "../Materi-9/HaloDunia";
// import Materi10 from "../Materi-10/ListUserInfo";
// import Materi11 from "../Materi-11/Timer";
// import Materi12 from "../Materi-12/DaftarPeserta";
// import Materi13 from "../Materi-13/DaftarPeserta";
// import HooksExample from "../Materi-13/HooksExample";
// import Materi14 from "../Materi-14/Movie";
// import Login from "../Materi-Authentication/Login";
// import Register from "../Materi-Authentication/Register";
// import Contestants from "../Materi-Authentication/Contestants";
// import SingleContestant from "../Materi-Authentication/SingleContestant";
import Nav from "../nav/Nav";
import Login from "../auth/Login";
import Register from "../auth/Register";
import Game from "../game/Game";
import Movie from "../movie/Movie";
import Home from "../home/Home";

export default function App() {
  return (
    <Router>
      <div>
        <Nav />
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          {/* contoh tag route tanpa end tag */}
          {/* <Route exact path="/" component={Home} /> */}

          {/* contoh dengan tag dengan end tag */}
          {/* <Route path="/materi-10">
            <Materi10 />
          </Route>
          <Route path="/materi-11">
            <Materi11 />
          </Route> */}

          <Route exact path="/game" component={Game} />
          <Route exact path="/movie" component={Movie} />
          {/* <Route exact path="/materi-13" component={Materi13} />
          <Route exact path="/materi-14" component={Materi14} /> */}
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/" component={Home} />
          {/* <Route exact path="/contestants" component={Contestants} />
          <Route exact path="/contestants/:id" component={SingleContestant} /> */}
        </Switch>
      </div>
    </Router>
  );
}
