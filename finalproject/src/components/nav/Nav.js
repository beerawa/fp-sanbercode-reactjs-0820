import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { AppBar, Toolbar, Button, Grid } from "@material-ui/core";
import { UserContext } from "../auth/UserContext";
import { useHistory } from "react-router-dom";

const Nav = () => {
  const styleLinkNavbar = {
    color: "inherit",
    textDecoration: "none",
  };
  let history = useHistory();

  const [user, setUser] = useContext(UserContext);

  const handleLogout = () => {
    setUser(null);
    localStorage.removeItem("user");
    history.push("/");
  };

  return (
    <AppBar position="static">
      <Toolbar>
        <Grid justify="space-between" container spacing={24}>
          <Grid item>
            {user === null && (
              <>
                <Button color="inherit">
                  <Link style={styleLinkNavbar} to="/">
                    Home
                  </Link>
                </Button>
              </>
            )}
            {user !== null && (
              <>
                <Button color="inherit">
                  <Link style={styleLinkNavbar} to="/">
                    Home
                  </Link>
                </Button>
                <Button color="inherit">
                  <Link style={styleLinkNavbar} to="/game">
                    Games
                  </Link>
                </Button>
                <Button color="inherit">
                  <Link style={styleLinkNavbar} to="/movie">
                    Movies
                  </Link>
                </Button>
              </>
            )}

            {/* <Button color="inherit">
              <Link style={styleLinkNavbar} to="/materi-12">Materi 12</Link>
            </Button>
            <Button color="inherit">
              <Link style={styleLinkNavbar} to="/materi-13">Materi 13</Link>
            </Button>
            <Button color="inherit">
              <Link style={styleLinkNavbar} to="/hooks-example">Hooks Example</Link>
            </Button>
            <Button color="inherit">
              <Link style={styleLinkNavbar} to="/materi-14">Materi 14</Link>
            </Button>
            <Button color="inherit">
              <Link style={styleLinkNavbar} to="/contestants">Contestants</Link>
            </Button> */}
          </Grid>

          <Grid item>
            <div>
              {user === null && (
                <>
                  <Button color="inherit">
                    <Link style={styleLinkNavbar} to="/login">
                      Login
                    </Link>
                  </Button>
                  <Button color="inherit">
                    <Link style={styleLinkNavbar} to="/register">
                      Register
                    </Link>
                  </Button>
                </>
              )}

              {user !== null && (
                <>
                  <Button color="inherit">{user.name}</Button>
                  <Button onClick={handleLogout} color="inherit">
                    Logout
                  </Button>
                </>
              )}
            </div>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
};

export default Nav;
