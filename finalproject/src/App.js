import React from "react";
import "./App.css";
import Routes from "./components/routes/Routes";
import { UserProvider } from "./components/auth/UserContext";

function App() {
  return (
    <div className="App">
      <UserProvider>
        <Routes />
      </UserProvider>
    </div>
  );
}

export default App;
